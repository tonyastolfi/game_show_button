/*
  Blink

  Turns an LED on for one second, then off for one second, repeatedly.

  Most Arduinos have an on-board LED you can control. On the UNO, MEGA and ZERO
  it is attached to digital pin 13, on MKR1000 on pin 6. LED_BUILTIN is set to
  the correct LED pin independent of which board is used.
  If you want to know what pin the on-board LED is connected to on your Arduino
  model, check the Technical Specs of your board at:
  https://www.arduino.cc/en/Main/Products

  modified 8 May 2014
  by Scott Fitzgerald
  modified 2 Sep 2016
  by Arturo Guadalupi
  modified 8 Sep 2016
  by Colby Newman

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/Blink
*/

const int redLED = 3;
const int blueLED = 10;
const int redButton = 11;
const int blueButton = 12;
const int buzzer = 2;

const int countDownDelayStepMillisec = 1000;
const int buzzerDelayMillisec = 2000;

void resetLEDs();

// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(buzzer, OUTPUT);
  for (int i=3; i<=10; ++i) {
    pinMode(i, OUTPUT);  
  }
  pinMode(redButton, INPUT_PULLUP);
  pinMode(blueButton, INPUT_PULLUP);

  resetLEDs();
}

void resetLEDs() {
  digitalWrite(redLED, LOW);    
  digitalWrite(blueLED, LOW);    
  delay(1000);
  
  digitalWrite(redLED, HIGH);    
  digitalWrite(blueLED, HIGH);    

  for (int j=0; j<4; ++j) {
    for (int i=4; i<=9; ++i) {
      digitalWrite(i, LOW);
    }
    delay(150);
    for (int i=4; i<=9; ++i) {
      digitalWrite(i, HIGH);
    }
    delay(150);
  }

  digitalWrite(redLED, LOW);    
  digitalWrite(blueLED, LOW);      
}

void countDown(int start, int finish, int dir) {
  int i;
  for (i = start; i != finish; i += dir) {
    digitalWrite(i, HIGH);
  }
  for (i = start; i != finish-dir; i += dir) {
    delay(countDownDelayStepMillisec);    
    digitalWrite(i, LOW);    
  }
  digitalWrite(buzzer, HIGH);
  delay(buzzerDelayMillisec);
  digitalWrite(buzzer, LOW);  
  digitalWrite(finish-dir, LOW);    
}

void redCountDown() {
  countDown(9, 2, -1);
}
void blueCountDown() {
  countDown(4, 11, +1);  
}

int blueButtonState[8] = {0};
int blueOnCount = 0;
int redButtonState[8] = {0};
int redOnCount = 0;

int tieBreak = 0;

// the loop function runs over and over again forever
void loop() {
  int blueWon = 0;
  int redWon = 0;
  for (int i=0; i<8; ++i) {
    
    redOnCount -= redButtonState[i];
    if (digitalRead(redButton) == LOW) {
      redButtonState[i] = 1;
    } else {
      redButtonState[i] = 0;
    }
    redOnCount += redButtonState[i];

    blueOnCount -= blueButtonState[i];
    if (digitalRead(blueButton) == LOW) {
      blueButtonState[i] = 1;
    } else {
      blueButtonState[i] = 0;
    }
    blueOnCount += blueButtonState[i];

    tieBreak = 1 - 0;

    if (redOnCount > 5) {
      if (tieBreak == 0 || !(blueOnCount > 5)) {
        redWon = 1;
        break;
      }
    }
    if (blueOnCount > 5) {
      blueWon = 1;
      break;
    }
    
    delay(1);
  }

  if (blueWon) {
    digitalWrite(redLED, LOW);        
    blueCountDown();
  } else if (redWon) {
    digitalWrite(blueLED, LOW);    
    redCountDown();
  } else {
    return;
  }

  redOnCount = 0;
  blueOnCount = 0;
  for (int i=0; i<8; ++i) {
    blueButtonState[i] = 0;
    redButtonState[i] = 0;
  }  

  resetLEDs();
}
